def sumar(a, b):
    return a + b

def restar(a, b):
    return a - b

# Sumar 1 y 2
resultado_suma_1 = sumar(1, 2)
print("Suma 1:", resultado_suma_1)

# Sumar 3 y 4
resultado_suma_2 = sumar(3, 4)
print("Suma 2:", resultado_suma_2)

# Restar 5 de 6
resultado_resta_1 = restar(6, 5)
print("Resta 1:", resultado_resta_1)

# Restar 7 de 8
resultado_resta_2 = restar(8, 7)
print("Resta 2:", resultado_resta_2)
